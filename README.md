### Joel Stutz's excellent nCloth Reference
Found on Marcus Ottosson's repository. The more sources available, the easiest it is for anyone to find it !

Originally [www.joelstutz.com/nCloth.html](https://web.archive.org/web/20140410011327/http://www.joelstutz.com/nCloth.html), but was taken down. Now it lives once more!

- https://mottosso.github.io/ncloth-reference
